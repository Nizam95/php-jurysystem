<?php
if (isset($_POST["submit"])) {

    if (addParticipant($_POST) > 0) {
        echo "
			<script>
				alert('Data has been Added !');
				document.location.href = 'index.php?page=view_participant';
			</script>
		";
    } else {
        echo "
			<script>
				alert('Data add failed !');
				document.location.href = 'index.php?page=view_participant';
			</script>
		";
    }
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <h1 class="mx-4 text-dark">ADD NEW PARTICIPANT</h1>
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
	<!-- /.content-header -->

	<!-- Main content -->
		<div class="container pt-2">

	<section class="content">
 <div class="col-12">
                <div class="card">

                    <div class="card-body">
			<form action="" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<label for="name">Name</label>
					<input type="text" name="nama" class="form-control" id="name" placeholder="Enter participant name" required />
				</div>

				<div class="form-group">
					<label for="code">Email</label>
					<input type="email" name="email" class="form-control"  aria-describedby="emailHelp" id="email" placeholder="Enter participant email" required />
                </div>

                <div class="form-group">
					<label for="code">Phone Number</label>
					<input type="number" name="phoneNumber" class="form-control" id="phoneNumber" placeholder="Enter participant phone Number" required />
                </div>

                <div class="form-group">
					<label for="code">Sport</label>
					<input type="sport" name="sport" class="form-control" id="sport" placeholder="Enter participant sport" required />
				</div>

				<br><br>
				<a href="index.php?page=view_participant" class="btn btn-secondary">Back</a>
				<button type="submit" name="submit" class="btn btn-primary">Save</button>
			</form>
		</div>
</div>
</div>
</div>
	</section>
	<!-- /.content -->
</div>
