<?php

if (!isset($_GET["id"])) {
    header("Location: index.php?page=dashboard.php");
    exit;

}

$id = $_GET["id"];

if (deleteParticipant($id) > 0) {
    echo "
		<script>
			alert('Data has been deleted !');
			document.location.href = 'index.php?page=view_participant';
		</script>
	";
} else {
    echo "
		<script>
			alert('Failed to delete participant !');
			document.location.href = 'index.php?page=view_participant';
		</script>
	";
}
