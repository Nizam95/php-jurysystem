<?php
require "conf/functions.php";

session_start();

if (!strpos($_SERVER['REQUEST_URI'], 'index.php')) {
    if (isset($_SESSION["login"])) {
        header("Location: index.php");
        exit;
    }
} else {
    if (!isset($_SESSION["login"])) {
        header("Location: main.php");
        exit;
    }

}
