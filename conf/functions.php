<?php
//? Connect a DB
$conn = mysqli_connect("localhost", "root", "", "jurysystemdb");

function query($query)
{
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row;
        }

    }
    return $rows;
}

function addParticipant($data)
{
    global $conn;

    $name = strtoupper($data["name"]);
    $email = strtolower(stripslashes($data["email"]));
    $phoneNumber = $data["phoneNumber"];
    $sport = strtoupper($data["sport"]);
    $query = "INSERT INTO participant VALUES ('', '$name', '$email', '$phoneNumber', '$sport')";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function assignParticipant($id, $juryId)
{
    global $conn;

    $query = "INSERT INTO jury_participant VALUES ('', '$juryId', '$id')";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);

}

function unassignParticipant($id)
{
    global $conn;
    mysqli_query($conn, "DELETE FROM jury_participant WHERE id = $id");
    return mysqli_affected_rows($conn);
}

function deleteParticipant($id)
{
    global $conn;
    mysqli_query($conn, "DELETE FROM participant WHERE id = $id");
    return mysqli_affected_rows($conn);
}

function deleteAdmin($id)
{
    global $conn;
    mysqli_query($conn, "DELETE FROM admin WHERE id = $id");
    return mysqli_affected_rows($conn);
}

function deleteJury($id)
{
    global $conn;
    mysqli_query($conn, "DELETE FROM jury WHERE id = $id");
    return mysqli_affected_rows($conn);
}

function updateParticipant($data)
{
    global $conn;

    $id = $data["id"];
    $name = strtoupper($data["name"]);
    $email = strtolower(stripslashes($data["email"]));
    $phoneNumber = $data["phoneNumber"];
    $sport = strtoupper($data["sport"]);

    $query = "UPDATE participant SET
				name = '$name',
				email = '$email',
				phoneNumber = '$phoneNumber',
				sport = '$sport'
			WHERE id = '$id'
			";

    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function updateAdmin($data)
{
    global $conn;

    $id = $data["id"];
    $name = strtoupper($data["name"]);
    $email = strtolower(stripslashes($data["email"]));
    $phoneNumber = $data["phoneNumber"];
    $query = "UPDATE admin SET
				name = '$name',
				email = '$email',
				phoneNumber = '$phoneNumber'
			WHERE id = '$id'
			";

    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function updateJury($data)
{
    global $conn;

    $id = $data["id"];
    $name = strtoupper($data["name"]);
    $institute = strtoupper($data["institute"]);
    $email = strtolower(stripslashes($data["email"]));
    $area = strtoupper($data["area"]);
    $phoneNumber = $data["phoneNumber"];

    $query = "UPDATE jury SET
				name = '$name',
				institute = '$institute',
				email = '$email',
                phoneNumber = '$phoneNumber',
				area = '$area'
			WHERE id = '$id'
			";

    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function register($data, $role)
{
    global $conn;
    if ($role == 'admin') {
        $name = strtoupper($data["name"]);
        $email = strtolower(stripslashes($data["email"]));
        $phoneNumber = $data["phoneNumber"];
        $password = mysqli_real_escape_string($conn, $data["password"]);

        $result = mysqli_query($conn, "SELECT email FROM admin WHERE email = '$email'");

        if (mysqli_fetch_assoc($result)) {
            $message = '<div class="callout callout-info">
                                    <h5><i class="fas fa-error"></i> ERROR:</h5>
                                 Email already exist.
                                    </div>';

            return $message;
        }

        $password = password_hash($password, PASSWORD_DEFAULT);

        mysqli_query($conn, "INSERT INTO admin  VALUES ('','$name', '$email', '$phoneNumber', '$password')");

        return mysqli_affected_rows($conn);

    } elseif ($role == 'jury') {
        $name = strtoupper($data["name"]);
        $institute = strtoupper($data["institute"]);
        $email = strtolower(stripslashes($data["email"]));
        $area = strtoupper($data["area"]);
        $phoneNumber = $data["phoneNumber"];
        $password = mysqli_real_escape_string($conn, $data["password"]);

        $result = mysqli_query($conn, "SELECT email FROM jury WHERE email = '$email'");

        if (mysqli_fetch_assoc($result)) {
            $message = '<div class="callout callout-info">
                                    <h5><i class="fas fa-error"></i> ERROR:</h5>
                                 Email already exist.
                                    </div>';

            return $message;
        }

        $password = password_hash($password, PASSWORD_DEFAULT);

        mysqli_query($conn, "INSERT INTO jury  VALUES ('','$name','$institute', '$email','$area', '$phoneNumber', '$password')");

        return mysqli_affected_rows($conn);
    }

}
