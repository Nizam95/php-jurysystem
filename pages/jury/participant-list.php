<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <h1 class="mx-4 text-dark">PARTICIPANT LIST</h1>
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">

<div class="container pt-2">
    <div class="row">
      <div class="col-12">
        <div class="card">
       
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone Number</th>
                  <th>Sport</th>
                </tr>
              </thead>
              <tbody>
                <?php
$i = 1;
$juryId = $_SESSION["currentUser"]["id"];
$participants = query("SELECT p.* FROM jury_participant jp LEFT JOIN participant p ON jp.participantId = p.id WHERE jp.juryId = $juryId");
foreach ($participants as $row): ?>
                  <tr>
                    <td><?=$i;?></td>
                    <td><?=$row['name'];?></td>
                    <td><?=$row['email'];?></td>
                    <td><?=$row['phoneNumber'];?></td>
                    <td><?=$row['sport'];?></td>
                  </tr>

                <?php $i++;
endforeach;?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div></div>
    </div>
  </section>
  <!-- /.content -->
</div>