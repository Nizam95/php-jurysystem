<!-- Content Wrapper. Contains page content -->
<?php 

$participant = query("SELECT * FROM participant");
$jury = query("SELECT * FROM jury");
$assigned = query("SELECT * FROM jury_participant");
$unassigned = query("SELECT * FROM participant p WHERE p.id NOT IN (SELECT participantId from jury_participant)");

$name = $_SESSION['currentUser']["name"];

?>

<div class="content-wrapper">
  <section class="content">
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
      <h1 class="px-4 py-4 text-dark">Hi <?= $name; ?>, Welcome to Dashboard</h1>
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
   
    <div class="container-fluid px-4">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?= count($participant); ?></h3>
              <p>PARTICIPANT</p>
            </div>
            <div class="icon">
              <i class="fas fa-user"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3><?= count($jury); ?></h3>
              <p>JURY</p>
            </div>
            <div class="icon">
          <i class="fas fa-user"></i>
            </div>
          </div>
        </div>

           <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-warning">
            <div class="inner">
              <h3><?= count($assigned); ?></h3>
              <p>ASSIGNED</p>
            </div>
            <div class="icon">
          <i class="fas fa-user"></i>
            </div>
          </div>
        </div>

          <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?= count($unassigned); ?></h3>
              <p>UNASSIGNED</p>
            </div>
            <div class="icon">
          <i class="fas fa-user"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->

  
        <!-- ./col -->
      </div>
    </div>
  </section>
</div>