<?php
$message = '';
$error = false;
require "conf/check-login.php";

if (isset($_GET['role'])) {
    $role = $_GET['role'];
} else {
    header("Location: main.php");
    exit;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["register"])) {
        $result = register($_POST, $role);
        if ($result > 0) {
            $_SESSION['success'] = '<div class="callout callout-success">
                                      <div class="row">
                                        <div class="col-8">
                                          <h5><i class="fas fa-check"></i> SUCCESS:</h5>
                                          Registration success.
                                        </div>
                                        <div class="col-4 btn-success btn-block  btn-lg text-center" role="button">
                                        <p><a class="text-white" href="main.php" style="text-decoration:none;">LOGIN</a></p>
                                        </div>
                                      </div>
                                    </div>';

            //redirect
            //  header("Location: main.php");
        } else {
            $_SESSION['message'] = $result;
            echo mysqli_error($conn);
        }
    }
}
?>

<head>
  <?php include "global/app.php";?>
  <?php include "includes/jquery.php";?>
  <link rel="stylesheet" href="css/main-style.css">
</head>


<body class="hold-transition register-page main">
  <div class="register-box">
     <div class="login-logo m-4">
     <h1 class="text-info"><?php echo ucwords($role); ?></h1>
    </div>

   <?php
if (isset($_SESSION['message'])) {
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}

if (isset($_SESSION['success'])) {
    echo $_SESSION['success'];
    unset($_SESSION['success']);
}
?>
    <div class="card">
      <div class="card-body register-card-body">
        <p class="login-box-msg">REGISTER NEW <?php echo strtoupper($role); ?></p>

       <form id="registerForm" method="post">
          <div class="input-group mb-3">
            <input type="text" class="form-control" name="name" placeholder="Full name" id="name">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <?php
if ($role == "jury") {
    echo '<div class="input-group mb-3">
                          <input type="text" class="form-control" name="institute" placeholder="Institute" id="institute">
                          <div class="input-group-append">
                            <div class="input-group-text">
                              <span class="fas fa-home"></span>
                            </div>
                          </div>
                        </div>';
}
?>

           <div class="input-group mb-3">
              <input type="tel" class="form-control" name="phoneNumber" placeholder="Phone Number" id="phoneNumber">
              <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-phone"></span>
              </div>
            </div>
          </div>

              <?php
if ($role == "jury") {
    echo '<div class="input-group mb-3">
            <input type="text" class="form-control" name="area" placeholder="Area" id="area">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-map"></span>
              </div>
            </div>
          </div>';
}
?>
          <div class="input-group mb-3">
            <input type="email" class="form-control" name="email" placeholder="Email"  id="email">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>

          <div class="input-group mb-3">
            <input type="password" class="form-control" name="password" placeholder="Password" id="password">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
               <div class="input-group mb-3">
            <input type="password" class="form-control" name="confirmPassword" placeholder="Confirm Password" id="confirmPassword">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <button type="submit" name="register" class="btn btn-primary btn-block">Register</button>
            </div>
            <!-- /.col -->
          </div>
        </form>

     <p class="mb-0 text-center">
          <a href="main.php" class="text-center">Already Registered?</a>
        </p>
      </div>
      <!-- /.form-box -->
    </div><!-- /.card -->
  </div>


    <script>
$(function () {
   $(document).ready(function () {
  $('#registerForm').validate({
    rules: {
      name:{
        required:true
      },

      email: {
        required: true,
        email: true,
      },
      phoneNumber:{
        required:true,
         number: true,
        minlength:9,
       maxlength: 12
      },

      password: {
        required: true,
        minlength: 6
      },
       confirmPassword : {
                   required: true,
        minlength: 6,
                    equalTo : "#password"
                },
      area:{
        required:true
      },
 institute:{
        required:true
      },
    },
    messages: {
      email: {
        required: "Please enter email address",
        email: "Please enter a vaild email address"
      },
        name: {
        required: "Please enter your full names",
      },
        institute: {
        required: "Please enter your institute",
      },
        area: {
        required: "Please enter your area",
      },
        phoneNumber: {
        required: "Please enter your phone number",
        number:"Invalid phone number",
        minlength: "Invalid phone number",
        maxLength:"Invalid phone number"
      },
      password: {
        required: "Please enter a password",
        minlength: "Your password must be at least 6 characters long"
      },
        confirmPassword: {
        required: "Please confirm your password",
        minlength: "Your password must be at least 6 characters long",
        equalTo:"Password not match"

      },


    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.input-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    },
     submitHandler: function (form) {
        form.submit();
      }
  });
});
 });
</script>
</body>

</html>