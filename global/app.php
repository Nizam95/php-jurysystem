<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Online Jury System</title>

<!-- Theme style -->
<link rel="stylesheet" href="dist/css/adminlte.min.css">

<!-- DEFAULT FONT -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

<!-- DEFAULT ICON -->
<link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">