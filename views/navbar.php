<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <div class="text-center display-2 bg-primary">
     <a href="#" class="brand-link" >
      <!-- <img src="../image/inteleclogo.png" alt="" class="brand-image img-circle elevation-3"
           style="opacity: .8"> -->
      <span class="brand-text font-weight-bold ">INTELEC 2020</span>
    </a>
  </div>


  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image" style=" align-self:center;">
        <i class="fas fa-user fa-2x  elevation-2"  style="color: cadetblue;"></i>
      </div>
      <div class="info">
       <div class="col-auto  font-weight-bold"> <a href="index.php?page=profile" class="d-block"><?php echo $_SESSION["currentUser"]["name"]; ?> </a></div>
       <div class="col-auto"><a  lass="d-block"><?php echo strtoupper($_SESSION["role"]); ?></a></div>
      <div class="col-auto"><a href="index.php?page=profile" lass="d-block">VIEW PROFILE</a></div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

              <!-- <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="fa fa-th nav-icon"></i>
                <p>Dashboard</p>
              </a>
            </li> -->
              <li class="nav-item">
              <a href="index.php?page=dashboard" class="nav-link">
                <i class="fa fa-th nav-icon"></i>
                <p>Dashboard</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="index.php?page=view_participant" class="nav-link">
                <i class="fa fa-th nav-icon"></i>
                <p>Participant List</p>
              </a>
            </li>

            <?php

if ($_SESSION["role"] == 'admin') {
    echo '<li class="nav-item">
              <a href="index.php?page=view_jury" class="nav-link">
                <i class="fa fa-th nav-icon"></i>
                <p>Jury List</p>
              </a>
            </li>';
}
?>


            <!-- <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="fa fa-th nav-icon"></i>
                <p>Category List</p>
              </a>
            </li> -->
            <li class="nav-item">
              <a href="./logout.php" class="nav-link">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>Log Out</p>
              </a>
            </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
