<?php
if (!isset($_GET["id"]) || !isset($_GET["juryId"])) {
    header("Location: index.php?page=dashboard.php");
    exit;

}


$id = $_GET["id"];
$juryId =$_GET["juryId"];
if (unassignParticipant($id) > 0) {
    echo "
		<script>
			alert('Data has been deleted !');
			document.location.href = 'index.php?page=jury_participant&id=$juryId';
		</script>
	";
} else {
    echo "
		<script>
			alert('Failed to delete participant !');
			document.location.href = 'index.php?page=jury_participant&id=$juryId';
		</script>
	";
}
