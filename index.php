
<!DOCTYPE html>
<?php
require "conf/check-login.php";

?>



<html lang="en">
<head>
<?php include "global/app.php";?>
<?php include "includes/styles.php";?>
<?php include "includes/jquery.php";?>
<?php include "includes/scripts.php";?>
</head>


<body class="hold-transition sidebar-mini layout-fixed">

  <div class="wrapper">

    <?php include "views/header.php";?>
    <?php include 'conf/page.php'?>

    <?php include "views/navbar.php";

    ?>
  
  </div>
    <?php include "views/controls.php";?>
    <?php include "views/footer.php";?>
</body>
</html>