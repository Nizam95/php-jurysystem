
<?php
require "conf/check-login.php";
$message = '';
$error = false;
$role = '';

if (isset($_GET['role'])) {
    $role = $_GET['role'];
} else {
    header("Location: main.php");
    exit;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["login"])) {

        $email = $_POST["email"];
        $password = $_POST["password"];

        $result = mysqli_query($conn, "SELECT * FROM $role WHERE email = '$email'");

        //? check username
        if (mysqli_num_rows($result) === 1) {

            //? check password
            $row = mysqli_fetch_assoc($result);
            if (password_verify($password, $row["password"])) {
                //? set session
                $_SESSION["login"] = true;
                $_SESSION["currentUser"] = $row;
                $_SESSION["role"] = $role;

                //? check remember me
                if (isset($_POST['remember'])) {
                    //? create cookie
                    setcookie('id', $row['id'], time() + 3600);
                    setcookie('key', hash('sha256', $row['email']), time() + 3600);
                    setcookie('role', $role, time() + 3600);
                }
                header("Location: index.php");

                exit;
            }else {
            $_SESSION['message'] = '<div class="callout callout-info">
                                    <h5><i class="fas fa-error"></i> ERROR:</h5>
                                    Invalid email or password.
                                    </div>';

            header("location: login.php?role=$role");
            exit;

        }
        } else {
            $_SESSION['message'] = '<div class="callout callout-info">
                                    <h5><i class="fas fa-error"></i> ERROR:</h5>
                                    Invalid email or password.
                                    </div>';

            header("location: login.php?role=$role");
            exit;

        }
        $error = true;
    }
}

?>

<head>
  <?php include "global/app.php";?>
  <?php include "includes/jquery.php";?>
  <link rel="stylesheet" href="css/main-style.css">
</head>


<body class="hold-transition login-page main">
  <div class="login-box">
    <div class="login-logo m-4">
     <h1 class="text-info"><?php echo ucwords($role); ?></h1>
    </div>
    <?php
if (isset($_SESSION['message'])) {
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}

?>
    <!-- /.login-logo -->
    <div class="card">
      <div class="card-body login-card-body">
        <p class="login-box-msg">Sign in to start</p>

        <form id="loginForm" method="post">
          <div class="input-group mb-3">
            <input type="email" class="form-control" name="email" placeholder="Email"  id="email">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="password" class="form-control" name="password" placeholder="Password" id="password">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-8">
              <div class="icheck-primary">
                <input type="checkbox" name="remember" id="remember">
                <label for="remember">
                  Remember Me
                </label>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-12 mt-2">
              <button type="submit" name="login"  value="Submit" class="btn btn-primary btn-block">Sign In</button>
            </div>
            <!-- /.col -->
          </div>
        </form>

        <p class="mb-0 text-center">
      <?php echo "<a href='register.php?role=$role' class='text-center'>Register a new membership</a>"; ?>
        </p>
      </div>


      <!-- /.login-card-body -->
    </div>

  </div>
  <!-- /.login-box -->


  <script>
$(function () {
   $(document).ready(function () {
  $('#loginForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 6
      },

    },
    messages: {
      email: {
        required: "Please enter email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 6 characters long"
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.input-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    },
     submitHandler: function (form) {
        form.submit();
      }
  });
});
 });
</script>

</body>

</html>