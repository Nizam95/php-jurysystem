<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <h1 class="mx-4 text-dark">PARTICIPANT LIST</h1>
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">

<div class="container pt-2">
    <div class="row">
      <div class="col-12">
        <div class="card">
  <div class="card-header">
            <a href="index.php?page=add_participant" class="btn btn-sm btn-primary btn-lg">Add New Participant</a>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone Number</th>
                  <th>Sport</th>
                  <th class="text-center">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
$i = 1;
$participants = query("SELECT * FROM participant");
foreach ($participants as $row): ?>
                  <tr>
                    <td><?=$i;?></td>
                    <td><?=$row['name'];?></td>
                    <td><?=$row['email'];?></td>
                    <td><?=$row['phoneNumber'];?></td>
                    <td><?=$row['sport'];?></td>
                     <td class="text-center">
                      <a href="index.php?page=update_participant&id=<?=$row["id"];?>" class="btn btn-sm btn-outline-warning"><i class="fa fa-edit"></i></a>
                      <a href="index.php?page=delete_participant&id=<?=$row["id"];?>" onclick="return confirm('Are you sure want to delete participant ?');" class="btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></a>
                    </td>
                  </tr>

                <?php $i++;
endforeach;?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div></div>
    </div>
  </section>
  <!-- /.content -->
</div>