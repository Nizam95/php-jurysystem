<?php
if (isset($_GET['page'])) {
$role = $_SESSION["role"];
  
  $page = $_GET['page'];
  switch ($page) {
      case 'profile':
        include 'pages/profile.php';
        break;

      case 'jury_participant':
        include "pages/$role/jury-participant-list.php";
        break;

      case 'assign_jury_participant':
        include "pages/$role/assign-jury-participant.php";
        break;

      case 'assign_participant':
        include "pages/$role/assign-participant.php";
        break;

      case 'unassign_participant':
        include "pages/$role/unassign-participant.php";
        break;

      case 'view_participant':
        include "pages/$role/participant-list.php";
        break;

      case 'delete_participant':
        include "pages/$role/delete-participant.php";
        break;

      case 'update_participant';
        include "pages/$role/update-participant.php";
        break;

      case 'add_participant';
        include "pages/$role/add-participant.php";
        break;

      case 'view_jury':
        include "pages/$role/jury-list.php";
        break;

      default:
      include "pages/dashboard.php";

  }
} else {
  include "pages/dashboard.php";
}
