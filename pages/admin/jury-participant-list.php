<!-- Content Wrapper. Contains page content -->

<?php
if (!isset($_GET["id"])) {
    header("Location: index.php?page=dashboard.php");
    exit;

}
$id = $_GET["id"];
$jury = query("SELECT name from jury WHERE jury.id = $id");
?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <h1 class="mx-4 text-dark">JURY PARTICIPANT LIST (
      <?php echo $jury[0]["name"]; ?>
        )</h1>
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">

<div class="container pt-2">
    <div class="row">
      <div class="col-12">
        <div class="card">
  <div class="card-header">
         <a href="index.php?page=assign_jury_participant&id=<?=$id;?>" class="btn btn-sm btn-primary btn-lg">Assign New Participant</i></a>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone Number</th>
                  <th>Sport</th>
                  <th class="text-center">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
$i = 1;
$participants = query("SELECT jp.*, p.*, jp.id as jpId FROM jury_participant jp LEFT JOIN participant p ON jp.participantId = p.id WHERE jp.juryId = $id");
foreach ($participants as $row): ?>
                  <tr>
                    <td><?=$i;?></td>
                    <td><?=$row['name'];?></td>
                    <td><?=$row['email'];?></td>
                    <td><?=$row['phoneNumber'];?></td>
                    <td><?=$row['sport'];?></td>
                     <td class="text-center">
                      <a href="index.php?page=unassign_participant&id=<?=$row["jpId"];?>&juryId=<?=$id?>" class="btn btn-warning">Unassign</i></a>
                    </td>
                  </tr>

                <?php $i++;
endforeach;
?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div></div>
    </div>
  </section>
  <!-- /.content -->
</div>