<!-- Content Wrapper. Contains page content -->
<?php
if (!isset($_GET["id"])) {
    header("Location: index.php?page=dashboard.php");
    exit;

}

$id = $_GET["id"];

?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <h1 class="mx-4 text-dark">ASSIGN PARTICIPANT</h1>
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">

<div class="container pt-2">
    <div class="row">
      <div class="col-12">
        <div class="card">

          <!-- /.card-header -->
          <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone Number</th>
                  <th>Sport</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
$i = 1;
$participants = query("SELECT * FROM participant WHERE participant.id NOT IN (SELECT participantId as id FROM jury_participant INNER JOIN jury ON jury_participant.juryId = jury.id WHERE jury_participant.juryId = $id)");
foreach ($participants as $row): ?>
                  <tr>
                    <td><?=$i;?></td>
                    <td><?=$row['name'];?></td>
                    <td><?=$row['email'];?></td>
                    <td><?=$row['phoneNumber'];?></td>
                    <td><?=$row['sport'];?></td>
                     <td class="text-center">
                      <a href="index.php?page=assign_participant&id=<?=$row["id"];?>&juryId=<?=$id?>" class="btn btn-sm btn-info">Assign</a>
                    </td>
                  </tr>

                <?php $i++;
endforeach;?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div></div>
    </div>
  </section>
  <!-- /.content -->
</div>