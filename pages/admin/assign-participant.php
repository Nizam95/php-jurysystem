<?php
if (!isset($_GET["id"]) || !isset($_GET["juryId"])) {
    header("Location: index.php?page=dashboard.php");
    exit;

}

$id = $_GET["id"];
$juryId = $_GET["juryId"];

if (assignParticipant($id, $juryId) > 0) {
    echo "
		<script>
			alert('Successfully assign.');
			document.location.href = 'index.php?page=assign_jury_participant&id=$juryId';
		</script>
	";
} else {
    echo "
		<script>
			alert('Failed to assign participant !');
		document.location.href = 'index.php?page=assign_jury_participant&id=$juryId';
		</script>
	";
}
