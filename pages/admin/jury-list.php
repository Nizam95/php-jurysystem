<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <h1 class="mx-4 text-dark">JURY LIST</h1>
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">

<div class="container pt-2">
    <div class="row">
      <div class="col-12">
        <div class="card">

          <!-- /.card-header -->
          <div class="card-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone Number</th>
                  <th>Institution</th>
                  <th>Area</th>
                  <th class="text-center">Participant</th>
                  <!-- <th>Action</th> -->
                </tr>
              </thead>
              <tbody>
                <?php
$i = 1;
$jury = query("SELECT * FROM jury");
foreach ($jury as $row): ?>
                  <tr>
                    <td><?=$i;?></td>
                    <td><?=$row['name'];?></td>
                    <td><?=$row['email'];?></td>
                    <td><?=$row['phoneNumber'];?></td>
                    <td><?=$row['institute'];?></td>
                        <td><?=$row['area'];?></td>
                        <td class="text-center">
                      <a href="index.php?page=jury_participant&id=<?=$row["id"];?>" class="btn btn-sm btn-info">VIEW</i></a>
                    </td>
                    <!-- <td class="text-center">
                      <a href="index.php?page=update_jury&id=<?=$row["id"];?>" class="btn btn-sm btn-outline-warning"><i class="fa fa-edit"></i></a>
                    </td> -->
                  </tr>

                <?php $i++;
endforeach;?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div></div>
    </div>
  </section>
  <!-- /.content -->
</div>