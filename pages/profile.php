<!-- Content Wrapper. Contains page content -->


<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row">
        <h1 class="mx-4 text-dark">PROFILE</h1>
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

<section class="content">

<div class="container pt-2">
        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="basicInfo-tab" data-toggle="tab" href="#basicInfo" role="tab" aria-controls="basicInfo" aria-selected="true">User Information</a>
                                    </li>
                                    <!-- <li class="nav-item">
                                        <a class="nav-link" id="connectedServices-tab" data-toggle="tab" href="#connectedServices" role="tab" aria-controls="connectedServices" aria-selected="false">Connected Services</a>
                                    </li> -->
                                </ul>
                                <div class="tab-content ml-1" id="myTabContent">
                                    <div class="tab-pane fade show active" id="basicInfo" role="tabpanel" aria-labelledby="basicInfo-tab">


                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Full Name</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                               <?php echo $_SESSION["currentUser"]["name"]; ?>
                                            </div>
                                        </div>
                                        <hr />

                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Phone Number</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                               <?php echo $_SESSION["currentUser"]["phoneNumber"]; ?>
                                            </div>
                                        </div>
                                        <hr />


                                        <div class="row">
                                            <div class="col-sm-3 col-md-2 col-5">
                                                <label style="font-weight:bold;">Email</label>
                                            </div>
                                            <div class="col-md-8 col-6">
                                               <?php echo $_SESSION["currentUser"]["email"]; ?>
                                            </div>
                                        </div>
                                        <hr />
                                       <?php 
                                       
                                       if($_SESSION["role"] == "jury"){
                                           $institute = $_SESSION['currentUser']['institute'];
                                           $area = $_SESSION['currentUser']['area'];

                                           echo "<div class='row'>
                                            <div class='col-sm-3 col-md-2 col-5'>
                                                <label style='font-weight:bold;'>Institution</label>
                                            </div>
                                            <div class='col-md-8 col-6'>
                                               $institute
                                            </div>
                                        </div>
                                        <hr />
                                        <div class='row'>
                                            <div class='col-sm-3 col-md-2 col-5'>
                                                <label style='font-weight:bold;'>Area/Category</label>
                                            </div>
                                            <div class='col-md-8 col-6'>
                                                $area 
                                            </div>
                                        </div>
                                        <hr />";
                                       }

                                       ?>

                                    </div>
                                    <!-- <div class="tab-pane fade" id="connectedServices" role="tabpanel" aria-labelledby="ConnectedServices-tab">
                                        Facebook, Google, Twitter Account that are connected to this account
                                    </div> -->
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
            </div>
        </div>
    </div>
  </section>
  <!-- /.content -->
</div>
